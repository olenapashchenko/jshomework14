const button = document.querySelector('.btn-theme')

button.addEventListener('click', () => {
    const changeTheme = localStorage.getItem('color-bg') === 'true';
    localStorage.setItem('color-bg', !changeTheme);
    document.body.classList.toggle('background-theme', !changeTheme);
});

document.addEventListener('DOMContentLoaded', () => {
    document.body.classList.toggle('background-theme', localStorage.getItem('color-bg') === 'true');
});